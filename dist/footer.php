<!-- FOOTER -->
<div class="footer">
		<div class="container">
			<div class="row">
				<div class="col-xl-2 col-lg-2 col-md-2 col-sm-6">
					<h5>меню</h5>
					<ul class="footer__nav">
						<li><a href="#">О компании</a></li>
						<li><a href="#">Торговые дома</a></li>
						<li><a href="#">Туры в хоргосе</a></li>
						<li><a href="#">Тур операторы</a></li>
						<li><a href="#">Контакты</a></li>
						<li><a href="#">Прочее</a></li>
					</ul>
				</div>
				<div class="col-xl-2 col-lg-2 col-md-2 col-sm-6">
					<h5>торговые дома</h5>
					<ul class="footer__nav">
						<li><a href="#">Кинг-Конг</a></li>
						<li><a href="#">Чжун Хэ</a></li>
						<li><a href="#">Цзянь Юань</a></li>
						<li><a href="#">Золотой Порт</a></li>
						<li><a href="#">Самрук</a></li>
					</ul>
				</div>
				<div class="col-xl-5 col-lg-5 col-md-8">
					<h5>подпишитесь на рассылку</h5>
					<form action="" class="subscription__form">
						<input class="subscription__value" type="email" placeholder="">
						<button class="subscription__button" type="button">Подписаться</button>
					</form>
					<h5 class="mt-3">мы в социальных сетях</h5>
					<ul class="social__btns mt-2">
						<li><a href="#">
						<img src="./images/v.png" alt="">
						</a></li>
						<li><a href="#">
						<img src="./images/y.png" alt="">
						</a></li>
						<li><a href="#">
						<img src="./images/t.png" alt="">
						</a></li>
						<li><a href="#">
						<img src="./images/v.png" alt="">	
						</a></li>
						<li><a href="#">
						<svg enable-background="new 0 0 40 40" version="1.1" viewBox="0 0 40 40" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
							<g>
								<g>
									<path d="M20,2c9.925,0,18,8.075,18,18s-8.074,18-18,18S2,29.925,2,20S10.075,2,20,2 M20,0C8.954,0,0,8.954,0,20    c0,11.047,8.954,20,20,20c11.044,0,20-8.953,20-20C40,8.954,31.045,0,20,0L20,0z" fill="#959595"/>
								</g>
								<path clip-rule="evenodd" d="M12.537,10.641h14.926c1.416,0,2.576,1.159,2.576,2.577v14.925   c0,1.418-1.16,2.577-2.576,2.577H12.537c-1.417,0-2.577-1.159-2.577-2.577V13.218C9.96,11.8,11.12,10.641,12.537,10.641   L12.537,10.641z M24.588,12.872c-0.498,0-0.904,0.407-0.904,0.904v2.164c0,0.497,0.406,0.903,0.904,0.903h2.27   c0.496,0,0.902-0.406,0.902-0.903v-2.164c0-0.497-0.406-0.904-0.902-0.904H24.588L24.588,12.872z M27.77,19.132h-1.768   c0.168,0.546,0.258,1.124,0.258,1.724c0,3.34-2.795,6.047-6.24,6.047c-3.447,0-6.243-2.707-6.243-6.047   c0-0.6,0.091-1.178,0.258-1.724h-1.844v8.482c0,0.439,0.358,0.799,0.798,0.799h13.983c0.438,0,0.797-0.359,0.797-0.799V19.132   L27.77,19.132z M20.02,16.73c-2.229,0-4.034,1.749-4.034,3.907c0,2.159,1.805,3.909,4.034,3.909c2.226,0,4.031-1.75,4.031-3.909   C24.051,18.479,22.246,16.73,20.02,16.73z" fill="#959595" fill-rule="evenodd"/>
							</g>
						</svg>
						</a></li>
					</ul>
					<div class="AStoreGPlay mt-5">
						<a href="#">
							<img src="./images/google.png" alt="">
						</a>
						<a href="#">
							<img src="./images/app.png" alt="">
						</a>
					</div>
				</div>
				<div class="col-xl-3 col-lg-3 pl-5 mobile__padding-left">
					<h5>Звоните</h5>
					<a class="phoneNumber" href="tel: 8 707 328 2299">8 707 328 2299</a>
					<button class="callOrder" data-toggle="modal" data-target="#callUs" type="button">Заказать звонок</button>
					<div class="mt-4">
						<h5>Адрес</h5>
						<p>Зона беспошлинной торговли между Казахстаном и Китаем</p>
					</div>
					<div class="mt-4">
						<h5>Время работы</h5>
						<p>Работаем с <span>7:00</span> до <span>19:00</span> без выходных.</p>
					</div>
					<!-- Модальное окно -->  
					<div class="modal fade" id="callUs" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
							<div class="modal-header">
							<h4 class="modal-title" id="myModalLabel">Свяжитесь с нами</h4>
							</div>
							<div class="modal-body">
								
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<nav id="catalog-nav">
		<ul class="first-nav">
			<li>
				<span>Навигация</span>
				<ul>
					<li><a href="#">о компании</></a></li>
					<li><a href="#">торговые дома</></a></li>
					<li><a href="#">тур операторы</></a></li>
					<li><a href="#">советы</></a></li>
					<li><a href="#">отзывы</></a></li>
					<li><a href="#">новости</></a></li>
					<li><a href="#">контакты</a></li>
				</ul>
			</li>
			<li class="clothes">
				<span>Одежда</span>
				<ul>
					<li class="man">
						<a href="#">Мужская</a>
						<ul>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
						</ul>
					</li>
					<li class="woman">
						<a href="#">Женская</a>
						<ul>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
						</ul>
					</li>
				</ul>
			</li>
			<li class="footwear">
				<span>Обувь</span>
				<ul>
					<li class="man">
						<a href="#">Мужская</a>
						<ul>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
						</ul>
					</li>
					<li class="woman">
						<a href="#">Женская</a>
						<ul>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
						</ul>
					</li>
				</ul>
			</li>
			<li class="linen">
				<span>Белье</span>
				<ul>
					<li class="man">
						<a href="#">Мужское</a>
						<ul>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
						</ul>
					</li>
					<li class="woman">
						<a href="#">Женское</a>
						<ul>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
							<li><a href="#">Контент</a></li>
						</ul>
					</li>
				</ul>
			</li>
			<li><a href="#">Большие размеры</a></li>
			<li><a href="#">Спецодежда</a></li>
			<li><a href="#">Одежда для офиса</a></li>
			<li><a href="#">Одежда для дома</a></li>
			<li><a href="#">Свадебные наряды</a></li>
			<li><a href="#">Будущие мамы</a></li>
			<li><a href="#">Все для пляжа</a></li>
		</ul>
    </nav>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.19/jquery.touchSwipe.min.js"></script>
	<script src="js/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="js/slick.min.js"></script>
    <script src="js/select2.min.js"></script>
	<script src="js/glide.min.js"></script>
	<script src="js/hc-offcanvas-nav.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/sweetalert2.min.js"></script>
	<script src="js/headhesive.min.js"></script>
	
    <script src="js/main.js"></script>
	<script>
		$('.leave__review-form').on('submit', function(e) {
			e.preventDefault();
		});
	</script>
	<script>
		const sorry = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			onOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});
		const success = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			onOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});
		const warning = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			onOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});
		const wrong = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			onOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});
		document.querySelector(".subscription__button").addEventListener('click', function(){
			if ($('.subscription__value').val() == '') {
				warning.fire({
					icon: 'warning',
					title: 'Введите ваш e-mail'
				});
			}
			else if($('.subscription__value').is(':valid')) {
				let request = new XMLHttpRequest();
				request.open('POST', 'http://dai5.kz/', true);
				request.send({
					email: $('.subscription__value').val()
				});
				request.ontimeout = function() {
					warning.fire({
						icon: 'info',
						title: 'Извините, запрос превысил максимальное время'
					});
				}
				if (request.responseText == 'ok') {
					success.fire({
						icon: 'success',
						title: 'Вы успешно подписались'
					});
				}
			}
			else {
				wrong.fire({
					icon: 'error',
					title: 'E-mail введен неправильно'
				});
			}
		});
	</script>
	<!-- FOOTER END -->
</body>
</html> 