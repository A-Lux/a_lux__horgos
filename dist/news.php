<?php
    require_once("header.php");
?>
    <div class="news">
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-7 pr-0">
                    <div class="flexible">
                        <a href="#">
                            <span>главная</span>
                        </a>
                        <span>/</span>
                        <h4>новости</h4>
                    </div>
                </div>
                <div class="col-xl-10 col-lg-9 col-md-8 col-sm-6 col-5"></div>
            </div>
        </div>
        <div class="news__wrapper">
            <div class="container">
                <div class="row mt-4 mb-4">
                    <div class="col-xl-4 col-md-4 pl-0 pr-0">
                        <img src="images/news__pic.png">
                    </div>
                    <div class="col-xl-8 col-md-8">
                        <div class="d-flex flex-column justify-content-between h-100 pt-3 pb-3">
                            <h1>Подарки всем кто с нами!!!</h1>
                            <p>
                                Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях. При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется абзацами случайным образом от двух до десяти предложений в абзаце
                            </p>
                            <div class="d-flex align-items-center">
                                <a href="#" class="more">Подробнее ...</a>
                                <span>16.06.2019 22:45</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4 mb-4">
                    <div class="col-xl-4 col-md-4 pl-0 pr-0">
                        <img src="images/news__pic.png">
                    </div>
                    <div class="col-xl-8 col-md-8">
                        <div class="d-flex flex-column justify-content-between h-100 pt-3 pb-3">
                            <h1>Подарки всем кто с нами!!!</h1>
                            <p>
                                Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях. При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется абзацами случайным образом от двух до десяти предложений в абзаце
                            </p>
                            <div class="d-flex align-items-center">
                                <a href="#" class="more">Подробнее ...</a>
                                <span>16.06.2019 22:45</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4 mb-4">
                    <div class="col-xl-4 col-md-4 pl-0 pr-0">
                        <img src="images/news__pic.png">
                    </div>
                    <div class="col-xl-8 col-md-8">
                        <div class="d-flex flex-column justify-content-between h-100 pt-3 pb-3">
                            <h1>Подарки всем кто с нами!!!</h1>
                            <p>
                                Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях. При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется абзацами случайным образом от двух до десяти предложений в абзаце
                            </p>
                            <div class="d-flex align-items-center">
                                <a href="#" class="more">Подробнее ...</a>
                                <span>16.06.2019 22:45</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4 mb-4">
                    <div class="col-xl-4 col-md-4 pl-0 pr-0">
                        <img src="images/news__pic.png">
                    </div>
                    <div class="col-xl-8 col-md-8">
                        <div class="d-flex flex-column justify-content-between h-100 pt-3 pb-3">
                            <h1>Подарки всем кто с нами!!!</h1>
                            <p>
                                Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях. При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется абзацами случайным образом от двух до десяти предложений в абзаце
                            </p>
                            <div class="d-flex align-items-center">
                                <a href="#" class="more">Подробнее ...</a>
                                <span>16.06.2019 22:45</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid favorite-boutiques">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 mt-5">
                        <h1 class="favorite-header">Избранные бутики</h1>
                    </div>
                </div>
                <div class="row favorite__slick">
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a href="#">
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                            <div class="boutique-block">
                            <a href="#">
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a href="#">
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a href="#">
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a href="#">
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a href="#">
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
    require_once("footer.php");
?>