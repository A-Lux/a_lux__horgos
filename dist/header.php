<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Хоргос</title>
    <link href="https://fonts.googleapis.com/css?family=Pacifico&display=swap" rel="stylesheet"> 
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css" integrity="sha384-KA6wR/X5RY4zFAHpv/CnoG2UW1uogYfdnP67Uv7eULvTveboZJg0qUpmJZb5VqzN" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/hc-offcanvas-nav@3.4.1/dist/hc-offcanvas-nav.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" href="https://cdn.bootcss.com/Glide.js/3.4.1/css/glide.theme.min.css">
    <link rel="stylesheet" href="https://cdn.bootcss.com/Glide.js/3.4.1/css/glide.core.min.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@9.4.3/dist/sweetalert2.min.css">
	<link rel="stylesheet" href="./css/main.css">
	
</head>
<body class="bg-grey">
    <!-- HEADER -->
    <div class="header">
        <div class="container pt-5 pb-4">
            <div class="row">
                <div class="col-xl-2 col-sm-4">
                    <a href="#" class="logo">Хоргос</a>
                </div>
                <div class="col-xl-8 col-sm-8">
                    <form action="" class="search-form">
					    <input class="search-field" type="search" placeholder="Поиск по продукции">
					    <input class="search-btn" type="submit" value="">
				    </form>
                    <p class="search-example">Например: <span>Женские меховые жилетки</span></p>
                </div>
                <div class="col-xl-2 col-sm-4">
                    <div class="loginOrReg">
						<a class="login-btn" href="#">Войти</a>
						<a class="reg-btn" href="#">Зарегистрироваться</a>
					</div>
                </div>
            </div>
        </div>
        <div class="multicolors">
            <div class="container">
                <div class="row">
                    <div class="col-xl-3">
                        <div class="burger-bc">

                        </div>
                        <div class="burger">
                            <a class="toggle">
                                <span></span>
                            </a>
                            <h3>Каталог бутиков</h3>
                        </div>
                    </div>
                    <div class="col-xl-9 flexible">
                        <ul class="main-nav">
                            <li><a href="#"><span>о компании</span></a></li>
                            <li><a href="#"><span>торговые дома</span></a></li>
                            <li><a href="#"><span>тур операторы</span></a></li>
                            <li><a href="#"><span>советы</span></a></li>
                            <li><a href="#"><span>отзывы</span></a></li>
                            <li><a href="#"><span>новости</span></a></li>
                            <li><a href="#"><span>контакты</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
	</div>
	
    <!-- PRODUCTION CATALOG -->
	
    <!-- PRODUCTION CATALOG END -->
    <!-- HEADER END -->