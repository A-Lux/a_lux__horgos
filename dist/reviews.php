<?php
    require_once("header.php");
?>
    <div class="reviews">
        <div class="container">
            <div class="row pt-5 mb-5">
                <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-7 pr-0">
                    <div class="flexible">
                        <a href="#">
                            <span>главная</span>
                        </a>
                        <span>/</span>
                        <h4>отзывы</h4>
                    </div>
                </div>
                <div class="col-xl-10 col-lg-9 col-md-8 col-sm-6 col-5"></div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-4 col-sm-6 d-flex align-items-center">
                    <h1>Наши клиенты о нас</h1>
                </div>
                <div class="col-xl-2 col-lg-3 col-sm-6">
                    <a href="#leave__review-wrapper" class="leave__review">
                        Оставить отзыв
                    </a>
                </div>
                <div class="col-xl-7 col-lg-5"></div>
            </div>
            <div class="row">
                <div class="col-xl-12 comment--wrapper">
                    <div class="row justify-content-between">
                        <div class="col-xl-2 col-lg-2">
                            <div class="avatar__wrapper">
                                <img src="images/ava.png">
                            </div>
                        </div>
                        <div class="col-xl-10 col-lg-10">
                            <div class="row">
                                <div class="col-xl-4">
                                    <div class="reviewBy">
                                        <h2>Александр <span>03.02.2019</span></h2>
                                        <h2>Оценка</h2>
                                        <div class="star-rating__wrapper">
                                            <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating" value="5">
                                            </label>
                                            <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                            </label>
                                            <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating" value="3">
                                            </label>
                                            <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating" value="2">
                                            </label>
                                            <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating" value="1">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p>По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст. 
                                В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и придаст неповторимый колорит советских времен.в мелкодисперсный аэрозоль и направляется вверх, где при контакте
                            </p>
                        </div>
                    </div>
                    <div class="row justify-content-end mt-3">
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-md-5 col-9">
                            <div class="useful__wrapper">
                                <h2>Отзыв полезен</h2>
                                <span><button class="liked" type="button"></button>5</span>
                                <span><button class="disliked" type="button"></button>5</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12 comment--wrapper">
                    <div class="row justify-content-between">
                        <div class="col-xl-2 col-lg-2">
                            <div class="avatar__wrapper">
                                <img src="">
                            </div>
                        </div>
                        <div class="col-xl-10 col-lg-10">
                            <div class="row">
                                <div class="col-xl-4">
                                    <div class="reviewBy">
                                        <h2>Александр <span>03.02.2019</span></h2>
                                        <h2>Оценка</h2>
                                        <div class="star-rating__wrapper">
                                            <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating" value="5">
                                            </label>
                                            <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                            </label>
                                            <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating" value="3">
                                            </label>
                                            <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating" value="2">
                                            </label>
                                            <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating" value="1">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p>По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст. 
                                В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и придаст неповторимый колорит советских времен.в мелкодисперсный аэрозоль и направляется вверх, где при контакте
                            </p>
                        </div>
                    </div>
                    <div class="row justify-content-end mt-3">
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-md-5 col-9">
                            <div class="useful__wrapper">
                                <h2>Отзыв полезен</h2>
                                <span><button class="liked" type="button"></button>5</span>
                                <span><button class="disliked" type="button"></button>5</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12 comment--wrapper">
                    <div class="row justify-content-between">
                        <div class="col-xl-2 col-lg-2">
                            <div class="avatar__wrapper">
                                <img src="">
                            </div>
                        </div>
                        <div class="col-xl-10 col-lg-10">
                            <div class="row">
                                <div class="col-xl-4">
                                    <div class="reviewBy">
                                        <h2>Александр <span>03.02.2019</span></h2>
                                        <h2>Оценка</h2>
                                        <div class="star-rating__wrapper">
                                            <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating" value="5">
                                            </label>
                                            <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                            </label>
                                            <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating" value="3">
                                            </label>
                                            <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating" value="2">
                                            </label>
                                            <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating" value="1">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p>По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст. 
                                В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и придаст неповторимый колорит советских времен.в мелкодисперсный аэрозоль и направляется вверх, где при контакте
                            </p>
                        </div>
                    </div>
                    <div class="row justify-content-end mt-3">
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-md-5 col-9">
                            <div class="useful__wrapper">
                                <h2>Отзыв полезен</h2>
                                <span><button class="liked" type="button"></button>5</span>
                                <span><button class="disliked" type="button"></button>5</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12 comment--wrapper">
                    <div class="row justify-content-between">
                        <div class="col-xl-2 col-lg-2">
                            <div class="avatar__wrapper">
                                <img src="">
                            </div>
                        </div>
                        <div class="col-xl-10 col-lg-10">
                            <div class="row">
                                <div class="col-xl-4">
                                    <div class="reviewBy">
                                        <h2>Александр <span>03.02.2019</span></h2>
                                        <h2>Оценка</h2>
                                        <div class="star-rating__wrapper">
                                            <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating" value="5">
                                            </label>
                                            <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                            </label>
                                            <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating" value="3">
                                            </label>
                                            <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating" value="2">
                                            </label>
                                            <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                                <input class="star-rating__input" type="radio" name="rating" value="1">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p>По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст. 
                                В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и придаст неповторимый колорит советских времен.в мелкодисперсный аэрозоль и направляется вверх, где при контакте
                            </p>
                        </div>
                    </div>
                    <div class="row justify-content-end mt-3">
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-md-5 col-9">
                            <div class="useful__wrapper">
                                <h2>Отзыв полезен</h2>
                                <span><button class="liked" type="button"></button>5</span>
                                <span><button class="disliked" type="button"></button>5</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="leave__review-wrapper" id="leave__review-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-4">
                        <h1>Оставьте отзыв о компании Хоргос, чтобы другие наши клиенты могли оценить сервис</h1>
                        <p>+ оставь отзыв и получи скидку 10% на следующую поездку</p>
                    </div>
                    <div class="col-xl-7 col-lg-8">
                        <form action="" class="leave__review-form">
                            <div class="row">
                                <div class="col-xl-4 col-md-4 pl-2 pr-2 pb-2">
                                    <input type="text" name="uname" placeholder="Имя" required oninvalid="this.setCustomValidity('Введите ваше имя')" oninput="setCustomValidity('')">
                                </div>
                                <div class="col-xl-4 col-md-4 pl-2 pr-2 pb-2">
                                    <input type="tel" name="utel" placeholder="Телефон" required oninvalid="this.setCustomValidity('Введите ваш номер телефона')" oninput="setCustomValidity('')">
                                </div>
                                <div class="col-xl-4 col-md-4 pl-2 pr-2 pb-2">
                                    <input type="email" name="umail" placeholder="Эл. почта" required oninvalid="this.setCustomValidity('Введите ваш email')" oninput="setCustomValidity('')">
                                </div>
                                <div class="col-xl-12 pl-2 pr-2 pt-2 pb-2">
                                    <textarea name="review__textarea" required placeholder="Оставьте отзыв" oninvalid="this.setCustomValidity('Введите ваш отзыв')" oninput="setCustomValidity('')"></textarea>
                                </div>
                                <div class="col-xl-4 col-md-4 pl-2 pr-2 pt-2">
                                    <div class="star-rating__wrapper">
                                        <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating" value="5">
                                        </label>
                                        <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating" value="4">
                                        </label>
                                        <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating" value="3">
                                        </label>
                                        <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating" value="2">
                                        </label>
                                        <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                            <input class="star-rating__input" type="radio" name="rating" value="1" required>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-3 col-sm-6 pl-2 pr-2">
                                    <label class="photo__attach">
                                        <input class="file" type="file">
                                        Прикрепить фото
                                    </label>
                                </div>
                                <div class="col-xl-5 col-md-5 col-sm-6 pl-2 pr-2">
                                    <div class="consent__wrapper">
                                        <input id="consent" type="checkbox" required oninvalid="this.setCustomValidity('Необходимо принять условие соглашения')" oninput="setCustomValidity('')">
                                        <label for="consent">Даю согласие на обработку <span>персональных данных</span></label>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-4 pl-2 pr-2 pt-2">
                                    <button type="submit" class="leave__review-btn">Оставить отзыв</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid favorite-boutiques">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 mt-5">
                        <h1 class="favorite-header">Избранные бутики</h1>
                    </div>
                </div>
                <div class="row favorite__slick">
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a href="#">
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                            <div class="boutique-block">
                            <a>
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a>
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a>
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a>
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a>
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid similar-boutiques">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 mt-5">
                        <h1 class="favorite-header">Похожие бутики</h1>
                    </div>
                </div>
                <div class="row similar__slick">
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a>
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                            <div class="boutique-block">
                            <a>
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a>
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a>
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a>
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a>
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid recommended-boutiques">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 mt-5">
                        <h1 class="favorite-header">Рекомендуемые бутики</h1>
                    </div>
                </div>
                <div class="row recommended__slick">
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a>
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                            <div class="boutique-block">
                            <a>
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a>
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a>
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a>
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a>
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
    require_once("footer.php");
?>