<?php
    require_once("header.php");
?>
    <div class="contact">
        <div class="container">
            <div class="row pt-5 mb-5">
                <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-7 pr-0">
                    <div class="flexible">
                        <a href="#">
                            <span>главная</span>
                        </a>
                        <span>/</span>
                        <h4>контакты</h4>
                    </div>
                </div>
                <div class="col-xl-10 col-lg-9 col-md-8 col-sm-6 col-5"></div>
            </div>
            <div class="row pb-5">
                <div class="col-xl-4 col-md-6 col-lg-5">
                    <div class="contact__wrapper">
                        <div class="contact__details--wrapper">
                            <div class="contact__details">
                                <h2>Алексей</h2>
                                <a href="tel: +7 (777) 126-67-94">+7 (777) 126-67-94</a>
                            </div>
                            <div class="contact__details">
                                <h2>Алексей</h2>
                                <a href="tel: +7 (777) 126-67-94">+7 (777) 126-67-94</a>
                            </div>
                            <div class="contact__details">
                                <h2>Адрес</h2>
                                <p>Казахстан, г Алматы,  пр. Сейфуллина, 170 б</p>
                            </div>
                        </div>
                        <div class="contact__card">
                            <h2>Генеральный директор:</h2>
                            <div class="card__wrapper">
                                <div class="img__wrapper">
                                    <img src="images/director.png">
                                </div>
                                <p>Алексей Николаевич Хоргосов</p>
                            </div>
                        </div>
                        <div class="contact__details--wrapper">
                            <h1>Отправить сообщения</h1>
                            <form action="" class="leave__message-form">
                                <input type="text" name="uname" placeholder="Имя" required oninvalid="this.setCustomValidity('Введите ваше имя')" oninput="setCustomValidity('')">
                                <input type="email" name="umail" placeholder="Email" required oninvalid="this.setCustomValidity('Введите ваш email')" oninput="setCustomValidity('')">
                                <textarea name="review__textarea" required placeholder="Сообщение" oninvalid="this.setCustomValidity('Введите ваше сообщение')" oninput="setCustomValidity('')"></textarea>
                                <button type="submit" class="leave__message-btn">Отправить</button>
                                <div class="consent__wrapper">
                                    <input id="consent" type="checkbox" required oninvalid="this.setCustomValidity('Необходимо принять условие соглашения')" oninput="setCustomValidity('')">
                                    <label for="consent">Даю согласие на обработку <span>персональных данных</span></label>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8 col-md-6 col-lg-7 pl-1 pr-1">
                    <div class="map__wrapper">
                        <h1>Общество с ограниченной ответственностью “Хоргос”</h1>
                        <div class="separator"></div>
                    </div>
                    <div class="mapouter"><div class="gmap_canvas"><iframe id="gmap_canvas" src="https://maps.google.com/maps?q=%D0%9E%D0%9E%D0%9E%20%E2%80%9C%D0%A5%D0%BE%D1%80%D0%B3%D0%BE%D1%81%E2%80%9D%2C%20%D0%90%D0%BB%D0%BC%D0%B0%D1%82%D1%8B&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div></div>
                </div>
            </div>
        </div>
        <div class="container-fluid favorite-boutiques">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 mt-5">
                        <h1 class="favorite-header">Избранные бутики</h1>
                    </div>
                </div>
                <div class="row favorite__slick">
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a href="#">
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                            <div class="boutique-block">
                            <a href="#">
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a href="#">
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a href="#">
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a href="#">
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-10">
                        <div class="boutique-block">
                            <a href="#">
                                <img src="images/boutique-oodji.png">
                            </a>
                            <h3 class="boutique-header">oodji</h3>
                            <p class="boutique-title">Женская одежда</p>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                            <a href="#">Перейти в бутик</a>
                            <p>Артикул: 100156321</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
    require_once("footer.php");
?>